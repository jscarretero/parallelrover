import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;

public class Rover extends Thread
{	
	private static int nextRoverId = 0;
	private int roverId;
	private int xCoord = 0;
	private int yCoord = 0;
	private Direction direction = Direction.N;
	private String commands;
	private Plateau plat;
	private boolean completed = false;
	private boolean halted = false;
	
	public Rover (int x, int y, Direction dir, Plateau plat, String comm) {
		this.xCoord = x;
		this.yCoord = y;
		this.direction = dir;
		this.commands = comm;
		this.plat = plat;
		this.roverId = nextRoverId++;
		checkRoverBoundaries(x,y);	
	}
	
	public void run() {
		Lock tmp = plat.obtainLock(xCoord, yCoord);
		if (tmp == null) {
			throw new IllegalArgumentException("Several Rovers in same plateau coordinate");
		}

		processCommands();
	}

	public void processCommands() {
		for (int i = 0; i < commands.length() && !halted; ++i) {
			char command = commands.charAt(i);
			process(command);
		}
		if (!halted) completed = true;		
	}
	
	public void process (char command) {
		switch (command) {
			case 'L':   turnLeft();	 break;
			case 'R':   turnRight(); break;
			case 'M':	move();   	 break;
			default:    throw new IllegalArgumentException("Wrong command received by rover");
		}
	}
		
	private void turnLeft() {
		// Not efficient, could use integer value as direction and decrement plus modulo 
		// to rotate anti-clockwise. 
		switch (direction) {
			case N: direction = Direction.W; break;
			case E: direction = Direction.N; break;
			case S: direction = Direction.E; break;
			case W: direction = Direction.S; break;			
		}
	}
	
	private void turnRight() {
		// Not efficient, could use integer value as direction and increment plus modulo 
		// to rotate clockwise. 
		switch (direction) {
			case N: direction = Direction.E; break;
			case E: direction = Direction.S; break;
			case S: direction = Direction.W; break;
			case W: direction = Direction.N; break;						
		}
	}
	private void move () {
		// Not checking if another Rover is in that coordinate.
		int next_yCoord = yCoord;
		int next_xCoord = xCoord;
		switch (direction) {
			case N:  next_yCoord++;  break;
			case E:  next_xCoord++;  break;
			case S:  next_yCoord--;  break;
			case W:  next_xCoord--; break;
			default: break; // not possible to jump here					
		}
		// Also, not checking if Rover is out of boundaries.
		checkRoverBoundaries(next_xCoord, next_yCoord);
		
		boolean done = false;
		while (!done) {
			//Try to acquire lock associated to new coordinate
			Lock tmp = plat.obtainLock(next_xCoord, next_yCoord);
			if (tmp != null) { //lock has been acquired
				plat.releaseLock(xCoord, yCoord);			
				xCoord = next_xCoord;
				yCoord = next_yCoord;
				done = true;			
			} else {
				// Detect if movement will be possible (blocking Rover has not finished commands), or there is deadlock (find cycle)
				// If so, finish thread, indicating halted
				// Otherwise, spin									
				// todo: not implemented
			}
		}
	}
	
	private void checkRoverBoundaries(int x, int y) { 
		if ((x < 0) || (y < 0) || (x > plat.width()) || (y > plat.height())) {
			throw new IllegalArgumentException("Rover is out of the boundaries");
		}					
	}
	
	public void printPosition() {
		System.out.println("" + Integer.toString(xCoord) + " " + Integer.toString(yCoord) + " " + direction.stringDir());
	}
	
}