public enum Direction {
	N("N"), E("E"), S("S"), W("W");
	
	private final String stringDir;	 	
	public String stringDir() {return stringDir;}
	Direction(String dir) {this.stringDir = dir;}
}