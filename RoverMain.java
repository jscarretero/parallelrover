import java.util.LinkedList;
import java.util.Scanner;
import java.util.List;

public class RoverMain {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
				
		String plateauData;
		String roverPos;
		String roverCommands;
		Plateau myPlateau;
		List<Rover> squad = new LinkedList<Rover>();
				
		//First read Plateau info.
		plateauData = scanner.nextLine();
		String[] plateauInfo = plateauData.split(" ");
		checkPlateauInfo(plateauInfo);
		myPlateau = new Plateau(Integer.parseInt(plateauInfo[0]), Integer.parseInt(plateauInfo[1]));
				
		//Then read Rover info
		while ( (roverPos = scanner.nextLine()) != null && (roverPos.length() != 0)) {
			try 
			{
				String[] roverInfo = roverPos.split(" ");				
				checkRoverInfo(roverInfo);
				
				int x = Integer.parseInt(roverInfo[0]);
				int y = Integer.parseInt(roverInfo[1]);
				Direction dir = Direction.N;				
				
				char d = roverInfo[2].charAt(0);								
				switch (d) {
					case 'N': dir = Direction.N; break; 
					case 'E': dir = Direction.E; break;
					case 'S': dir = Direction.S; break;
					case 'W': dir = Direction.W; break;
					default: /*Already checked*/ break; 
				}
				
				roverCommands = scanner.nextLine();
				squad.add(new Rover(x, y, dir, myPlateau, roverCommands)); //garbage collector will easily take care
				//squad.add(myRover);
			} catch (Exception e) {
				System.out.println("Exiting: wrong input line");
				System.exit(1);
			}
		}
		
		for (Rover tmp : squad) {
			tmp.start();
		}
		
		for (Rover tmp : squad) {
			try { tmp.join();} 
			catch (Exception e) {/*No thread is interrupting another, this should not happen*/}
		}

		for (Rover tmp : squad) {
			tmp.printPosition();
		}
								
		scanner.close();
		System.exit(0);
		
	}	
	
	private static void checkPlateauInfo(String[] plateauInfo) {
		// Would check for: 2 strings in plateauInfo
		// Both are positive integers
	}
	private static void checkRoverInfo(String[] roverInfo) {
		// Would check for: 3 strings in roverInfo
		// First two are positive integers
		// Last is a valid direction 
	}	
}





