import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Plateau
{
	private int width = 0;
	private int height = 0;
	private ReentrantLock[][] sync;
	
	public Plateau (int width, int height) {
		this.width = width;
		this.height = height;
		checkDimensions();
		sync = new ReentrantLock[width+1][height+1];
	
		for (int i = 0; i < width + 1; ++i)
			for (int j = 0; j < height + 1; ++j)
				sync[i][j] = new ReentrantLock();				
	}
	
	private void checkDimensions() {
		if ((width < 0) || (height < 0)) {
			throw new IllegalArgumentException("Wrong plateau dimensions");
		}
	}
	
	public Lock obtainLock (int x, int y) {
		if ((x < 0) || (y < 0) || (x > width) || (y > height)) {
			throw new IllegalArgumentException("Wrong plateau coordinate");
		}
		boolean acquired = sync[x][y].tryLock();
		if (acquired) {			
			return sync[x][y];
		} else {
			return null;
		}
	}
	
	public void releaseLock (int x, int y) {
		if ((x < 0) || (y < 0) || (x > width) || (y > height)) {
			throw new IllegalArgumentException("Wrong plateau coordinate");
		}
		if (sync[x][y].isHeldByCurrentThread()) {
			sync[x][y].unlock();
		}
	}
		
	public int width()  {return width;}
	public int height() {return height;}		
}
